(function(){
    const nodoSection = document.querySelector("#contenido>section");
    fetch("/backend/discos.json")
    .then(r=>r.json())
    .then(data=>{
        var fragmento = document.createDocumentFragment();
        data.discos.forEach(disco => {
            var div = document.createElement("div");
            div.setAttribute("id","disco-"+disco.id);
            div.classList.add("discocard");
            div.classList.add("col-sm-4");

            var nom = document.createElement("div");
            nom.innerText = disco.titulo +` (${disco.año})`;
             
            var img = document.createElement("img");
            img.setAttribute("width","160px");
            img.setAttribute("src",disco.arteTapa);

            div.addEventListener('click',e => {
                let elem = e.target.classList.contains("discocard") ? e.target : e.target.parentNode;
            });
            div.appendChild(nom);
            div.appendChild(img);
            fragmento.appendChild(div);
        });
        nodoSection.appendChild(fragmento);
    });
})();