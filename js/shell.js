window.addEventListener('load', function(){
    let url = location.pathname.replace('/','');
    if(!url){
        url = "home";
    }
    cargadorPage(RUTAS[url]);
});


//felchas de nav
window.addEventListener('popstate', e=>{
    var estado;
    if(e.state){
        estado = e.state;
    } else {
        estado = RUTAS.home;
    }
    cargadorPage(estado);
});

document.querySelectorAll(".linked").forEach(btn=>{
    btn.addEventListener('click', e => {
        let url = btn.dataset.ruta;
        history.pushState(RUTAS[url],null,url);
        cargadorPage(RUTAS[url]);
    });
});

function cargadorPage(pageData){

    if(!pageData){
        pageData = RUTAS.error404;
    }
    
    const xhr = new XMLHttpRequest();
    xhr.open('GET', 'pages/' + pageData.pagina);
    xhr.send();
    xhr.addEventListener('load', e =>{
        document.getElementById("contenido").innerHTML = xhr.response;
        document.querySelector("title").innerHTML = pageData.titulo;

        //en caso que tengan script
        if("codigo" in pageData){
            const nodoScript = document.createElement('script');
            nodoScript.src = "js/"+pageData.codigo;
            document.getElementById("contenido").appendChild(nodoScript);
        }

        //en caso que tengan estilo
        if("estilo" in pageData){
            const nodoLink = document.createElement('link');
            nodoLink.rel = "stylesheet";
            nodoLink.href = "css/"+pageData.estilo;
            document.querySelector("head").appendChild(nodoLink);
        }
    });
};