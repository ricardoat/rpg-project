const RUTAS = {
    'home':{
        'pagina':'home.html',
        'titulo':'Música',
        'estilo':'shell.css'
    },

    'discos':{
        'pagina':'discos.html',
        'titulo':'Discos de Música',
        'codigo':'discos.js',
        'estilo':'shell.css'
    },

    'relatos':{
        'pagina':'relatos.html',
        'titulo':'Relatos',
        'estilo':'shell.css'
    },

    'error404':{
        'pagina':'404.html',
        'titulo':'No Encontrado'
    }
};